#!/bin/sh
if [ ! -f /.initialized_afp ]
then
  adduser -u 2000 -D -H backup
  echo 'backup:'"$EPASSWD"'' | chpasswd -e
  touch /.initialized_afp
fi

exec netatalk -d
