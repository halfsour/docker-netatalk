# README #

Create a docker image that hosts netatalk for Time Machine to use on a Mac for its backups

### Requirements ###
* Provide an afp.conf file that netatalk will use as its configuration
* Provide a salted, hashed password for the user "backup" that is created
* Provide a share from the host OS's filesystem

### Running ###
docker run -d -p 548:548/tcp -v /path/to/afp.conf:/etc/afp.conf -v /path/to/data/:/data/ -e EPASSWD='$1$xyz$cEUv8aN9ehjhMXG/kSFnM1' halfsour/netatalk-docker

#### Creating a salted hash for the password ####
openssl passwd -1 -salt xyz  yourpassword